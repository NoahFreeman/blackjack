from random import shuffle
from os import system

def C():    #clear termenal
    system('clear')

def create_deck():
    suits = ['♡', '♢', '♧', '♤']
    ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    deck = [{'rank': rank, 'suit': suit} for rank in ranks for suit in suits]
    shuffle(deck)
    return deck

def calculate_score(hand):
    score = 0
    num_aces = 0
    for card in hand:
        if card['rank'].isdigit():
            score += int(card['rank'])
        elif card['rank'] in ['J', 'Q', 'K']:
            score += 10
        else:
            num_aces += 1
            score += 11
    while score > 21 and num_aces > 0:
        score -= 10
        num_aces -= 1
    return score

def display_hand(hand):
    for card in hand:
        print(f"{card['rank']} {card['suit']}")
        
        

def main(): #main game loop
    
    with open('GameLog.txt','w') as log:    #clear log
        log.write('')
        
    log = open('GameLog.txt','a')   #open log in append mode
    C()

    initbalance = int(input('How much money do you have? $'))
    log.write(f'Initial balance: ${initbalance}.\n')
    balance = initbalance

    while True:
        C()

        if balance <= 0:
            print('You have no money!')
            log.write(f'Player has no money.\n')
            break
        
        bet = 0
        while bet <= 0 or bet > balance:
            try:
                bet = int(input(f'Place your bet ($1 to ${balance}): $'))
            except ValueError:
                C()
                print("Please enter a valid bet.")
        log.write(f'Player bets ${bet}.\n')

        deck = create_deck()
        player_hand = [deck.pop(), deck.pop()]
        log.write(f'Player\'s hand: {player_hand}.\n')
        dealer_hand = [deck.pop(), deck.pop()]
        log.write(f'Dealer\'s hand: {dealer_hand}.\n')

        print("Dealer's Hand:")
        print("? ?")
        print(f"{dealer_hand[1]['rank']} {dealer_hand[1]['suit']}")
        print("\nYour Hand:")
        display_hand(player_hand)
        player_score = calculate_score(player_hand)
        while player_score < 21:
            action = input("\nDo you want to (h)it or (s)tand? ").lower()
            
            if action == 'h':
                log.write('Player hits.\n')
                
                player_hand.append(deck.pop())
                log.write(f'Player\'s new hand: {player_hand}.\n')
                
                print("\nYour Hand:")
                display_hand(player_hand)
                player_score = calculate_score(player_hand)
                if player_score > 21:
                    log.write(f'Player busted\n')
                    print("\nBusted! You lose.")
                    balance -= bet
                    break
            elif action == 's':
                log.write('Player stands.\n')
                break

        if player_score <= 21:
            dealer_score = calculate_score(dealer_hand)
            print("\nDealer's Hand:")
            display_hand(dealer_hand)
            while dealer_score < 17:
                dealer_hand.append(deck.pop())
                log.write('Dealer hits.\n')
                log.write(f'Dealer\'s new hand: {dealer_hand}.\n')
                print("\nDealer hits...")
                print(f"{dealer_hand[-1]['rank']} {dealer_hand[-1]['suit']}")
                dealer_score = calculate_score(dealer_hand)
            
            if dealer_score > 21:
                log.write('Dealer busted.\n')
                print("\nDealer busts! You win!")
                balance += bet
            elif dealer_score > player_score:
                log.write('Dealer wins.\n')
                print("\nDealer wins!")
                balance -= bet
            elif player_score > dealer_score:
                log.write('Player wins.\n')
                print("\nYou win!")
                balance += bet
            else:
                print("\nIt's a tie!")
                log.write('Dealer and player tie.\n')
        
        print(f"Your balance: ${balance}")
        print(f'Your total winnings: ${balance-initbalance}')
        log.write(f'Player\'s balance: {balance}.\n')
        

        if input('Do you want to (q)uit or keep (p)laying? ') == 'q':
            break
    log.write('Game end.')
    log.close
    

    if input('Would you like to (v)iew the log file for this game or (q)uit? ') == 'v':
        C()
        with open('GameLog.txt','r') as log:
            for line in log:
                print(line,end='')

main()
